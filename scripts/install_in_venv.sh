#!/usr/bin/env bash
set -euo pipefail

SELF=$(readlink -f "${BASH_SOURCE[0]}")
LOCAL_DIR=${SELF%/*/*}
VENV_DIR=$(readlink -f "${1:-$LOCAL_DIR/venv}")

# Create and activate the virtual environment before running CMake.
mkdir -p "$VENV_DIR"
python3 -m venv "$VENV_DIR"
source "$VENV_DIR/bin/activate"
pip install -U pip
pip install -U numpy

# Build the project.
mkdir -p "${LOCAL_DIR}/build"
cd "${LOCAL_DIR}/build"
# The project hasn't been updated since 2016 so the internal configuration for
# downloading the testing library is no longer valid.
# TODO: Fix this is possible.
cmake -Dtests=OFF ..
cmake --build . -j "$(nproc)"

# Install the package directly into the virtual environment.
cmake --install . --prefix "$VENV_DIR"

# Generate an environment file.
LD_LIBRARY_PATH=$(readlink -f "$VENV_DIR/lib")
PYTHONPATH=$(readlink -f "$VENV_DIR/lib/python"*"/site-packages")

cat > "$VENV_DIR.sh" << ENV
#!/usr/bin/env bash
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH@Q}
export PYTHONPATH=${PYTHONPATH@Q}
source ${VENV_DIR@Q}/bin/activate
ENV

cat << USAGE
DCPROGS has been installed in $VENV_DIR.
To use it, run the following command in a terminal before running your code.

  source ${VENV_DIR@Q}.sh

USAGE
